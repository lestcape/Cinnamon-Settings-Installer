Cinnamon-Settings-Installer
===========================

A module for Cinnamon Settings, to add new features to the Applets, Desklets, Extensions and Themes...

This project it's in progress there are not any release. Used for tested propouse only.

The code is base on the code of Cinnamon Settings with only some minors modification for now... Cinnamon settings have an interesting design that allow you to create your own side page, and they can be a part of the settings... 

![](Cinnamon-Settings-Installer/Capture.png)


Motivation Project:
===========================
The intention it's not break the current Cinnamon functionality, not replace, copy or move any code of Cinnamon. It's make more options, modify behaviors or any other thing, without create a new unsupported branch, damages the image of Cinnamon, or forced the user to use this tools in any way or any form. But we want to have alternatives always.

- Cinnamon Settings is constructed to be easy to use, but some user don't like an easy thing... We like a faster thing with less click, that will be accessible always without need to go back and then enter in other session for only do the same thing... handle the spices applets, desklets, extensions...
- Cinnamon Settings still continues using gksudo instead of pkexec, and this is obsolete...
- Cinnamon Settings, don't have a multithreading way for download the metadata of applets desklet, extentions... And this can implemented more faster in a multithreading enviroment.
- Cinnamon Settings can not handle dependencies for the hard packages (system packages), and some applets or desklets could have dependencies that can be resolved.
- Cinnamon Settings do not allow you to know if an specific applet could work in the specific Cinnamon version that you have, and some applet could break down Cinnamon, because they are not update to work in the new Cinnamon version for example. Also we can have an alternative database with patched for the not working extentions in some external place. This could be kept by external users, and will can apply the patches for the not working extentions in the installation process, that will be transparent for the final users.
- Cinnamon Settings not provide a service/terminal/a way/ to an external application for install some extension if they want or need to do that.
- Cinnamon Settings not allow a local installation: If you have an applet, desklet... with translation, or a gsetting schema, you will requiere to do an installation. If you copy directly the applet to the corresponding folder, this will not work with translation or you could not access to the settings... there are guys without internet connections that can not perform an install from spices web site.. this guys can not use this applet or at less the applet can not be translate for him.
- Cinnamon Settings don't have a cron.d or a service to call periodically and inform the user for any update of the installed applets, desklets, extentions and themes.
- Cinnamon settings have a powerfull way to build the settings for applets, desklets and extentions using only a json file, but do not exploit all potential that could have. In some contexts, we need more actions that are not available in Cinnamon Settings...


Install Instruction:
===========================
1- Close all Settings Instances

2- Extract the archive

3- Open a terminal and go to the folder Cinnamon-Settings-Installer/settings-module: cd "path"

4- Add permisions to execute the file install.py

5- Execute the file as root: "sudo ./install.py"

Uninstall Instruction:
===========================
1- Close all Settings Instances

2- Extract the archive

3- Open a terminal and go to the folder Cinnamon-Settings-Installer/settings-module: cd "path"

4- Add permisions to execute the file uninstall.py

5- Execute the file as root: "sudo  ./uninstall.py"

