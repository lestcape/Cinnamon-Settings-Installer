#!/usr/bin/env python

import sys

try:
    import os
    import shutil
except Exception, detail:
    print detail
    sys.exit(1)

PATH_INSTALLER_MODULES = "/usr/lib/cinnamon-settings/installer_modules"
PATH_MODULES = "/usr/lib/cinnamon-settings/modules/"
PATH_BIN = "/usr/lib/cinnamon-settings/bin/"
PATH_ICONS = "/usr/share/icons/hicolor/scalable/categories/"

MODULES = ["cs_installer.py"]
LIBS = ["ExtensionInstallerCore.py", "XletInstallerSettings.py", "XletInstallerSettingsWidgets.py", "XletInstallerModules.py"]
BUILDERS = ["manager.ui"]
ICONS = ["cs-cinnamon-installer.svg"]


if os.path.exists(PATH_INSTALLER_MODULES):
   shutil.rmtree(PATH_INSTALLER_MODULES)

for module in MODULES:
    out = os.path.join(PATH_MODULES, module)
    if os.path.isfile(out):
        os.remove(out)
    out = os.path.join(PATH_MODULES, module + "c")
    if os.path.isfile(out):
        os.remove(out)

for lib in LIBS:
    out = os.path.join(PATH_BIN, lib)
    if os.path.isfile(out):
        os.remove(out)
    out = os.path.join(PATH_BIN, lib + "c")
    if os.path.isfile(out):
        os.remove(out)

for builder in BUILDERS:
    out = os.path.join(PATH_BIN, builder)
    if os.path.isfile(out):
        os.remove(out)

for icon in ICONS:
    out = os.path.join(PATH_ICONS + icon)
    if os.path.isfile(out):
        os.remove(out)

#now test that
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py")
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py applets configurableMenu@lestcape configurableMenu@lestcape")

