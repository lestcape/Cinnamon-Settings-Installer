#!/usr/bin/env python

import sys

try:
    import py_compile
    import os
    import shutil
except Exception, detail:
    print detail
    sys.exit(1)

ABS_PATH = os.path.abspath(__file__)
DIR_PATH = os.path.dirname(ABS_PATH) + "/"

PATH_GUI = os.path.join(os.path.dirname(os.path.dirname(ABS_PATH)), "gui")
PATH_ICON = os.path.join(PATH_GUI, "img")

PATH_INSTALLER_MODULES = "/usr/lib/cinnamon-settings/installer_modules/"
PATH_MODULES = "/usr/lib/cinnamon-settings/modules/"
PATH_BIN = "/usr/lib/cinnamon-settings/bin/"
PATH_ICONS = "/usr/share/icons/hicolor/scalable/categories/"

INSTALLER_MODULES = ["cs_applets_replace.py", "cs_desklets_replace.py", "cs_extensions_replace.py", "cs_themes_replace.py"]
MODULES = ["cs_installer.py"]
LIBS = ["ExtensionInstallerCore.py", "XletInstallerSettings.py", "XletInstallerSettingsWidgets.py", "XletInstallerModules.py", "InstallerProviders.py", "ExecuterSpi.py"]
BUILDERS = ["manager.ui", "main.ui"]
ICONS = {"cinnamon.svg": "cs-cinnamon-installer.svg", "cs-xlet-error.svg":"cs-xlet-error.svg"}


for module in INSTALLER_MODULES:
    src = os.path.join(os.path.join(DIR_PATH, "installer_modules"), module)
    out = os.path.join(PATH_INSTALLER_MODULES, module)
    if not os.path.exists(PATH_INSTALLER_MODULES):
        os.makedirs(PATH_INSTALLER_MODULES)
    shutil.copy(src, out)
    os.chmod(out, 0644)
    py_compile.compile(out)

for module in MODULES:
    src = os.path.join(DIR_PATH, module)
    out = os.path.join(PATH_MODULES, module)
    if not os.path.exists(PATH_MODULES):
        os.makedirs(PATH_MODULES)
    shutil.copy(src, out)
    os.chmod(out, 0644)
    py_compile.compile(out)

for lib in LIBS:
    src = os.path.join(DIR_PATH + lib)
    out = os.path.join(PATH_BIN, lib)
    if not os.path.exists(PATH_BIN):
        os.makedirs(PATH_BIN)
    shutil.copy(src, out)
    os.chmod(out, 0644)
    py_compile.compile(out)

for builder in BUILDERS:
    src = os.path.join(PATH_GUI, builder)
    out = os.path.join(PATH_BIN, builder)
    if not os.path.exists(PATH_BIN):
        os.makedirs(PATH_BIN)
    shutil.copy(src, out)
    os.chmod(out, 0644)

for icon in ICONS:
    src = os.path.join(PATH_ICON, icon)
    out = os.path.join(PATH_ICONS + ICONS[icon])
    if not os.path.exists(PATH_ICONS):
        os.makedirs(PATH_ICONS)
    shutil.copy(src, out)
    os.chmod(out, 0644)

#now test that
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py")
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py desklets")
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py applets configurableMenu@lestcape configurableMenu@lestcape")
#os.system("python /usr/lib/cinnamon-settings/cinnamon-settings.py applets configurableMenu@lest")
